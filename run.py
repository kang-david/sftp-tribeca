import os
from dotenv import load_dotenv
import paramiko
import pandas as pd
import datetime

# Read from .env file.
load_dotenv()
env = os.environ.get

# Create SSH client.
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())




# Establish SFTP connection to GLI's remote server.
ssh.connect(hostname=env("GLI_HOSTNAME"), username=env("GLI_USERNAME"), password=env("GLI_PASSWORD"), port=22)
print("SSH connection to GLI complete.")
sftp_client = ssh.open_sftp()
print("SFTP connection to GLI complete.")

# Download Vector Health's dropoff file as "vector_file.csv".
sftp_client.get(env("GLI_FILE_PATH"), "vector_file.xlsx")
print("Successfully downloaded 'vector_file.csv'.")

# Close SFTP connection to GLI remote server.
sftp_client.close()
print("SFTP connection to GLI terminated.")
ssh.close()
print("SSH connection to GLI terminated.")



# Establish SFTP connection to Tribeca's remote server.
ssh.connect(hostname=env("TRIBECA_HOSTNAME"), username=env("TRIBECA_USERNAME"), password=env("TRIBECA_PASSWORD"), port=22)
print(f"SSH connection to Tribeca complete.")
sftp_client = ssh.open_sftp()
print(f"SFTP connection to Tribeca complete.")

# Download Tribeca's dropoff file as "tribeca_file.csv".
sftp_client.chdir(env("TRIBECA_HOME_DIR"))
sftp_client.get(env("TRIBECA_FILE_PATH"), "tribeca_file.csv")
print("Successfully downloaded 'tribeca_file.csv'.")

# Close SFTP connection to Tribeca's remote server.
sftp_client.close()
print(f"SFTP connection to Tribeca terminated.")
ssh.close()
print(f"SSH connection to Tribeca terminated.")



# Create Pandas dataframes with downloaded files.
df_vector = pd.read_excel(r"vector_file.xlsx")
df_tribeca = pd.read_csv(r"tribeca_file.csv")



# Creating df that will be the first output file.
df_output_1 = df_vector

# Create function that takes two columns (date & time) and creates a new single date_time column.
def get_datetime(date, time):
    date_time = date.strftime("%Y-%m-%d") + " " + str(time)
    return date_time

# Apply above function.
df_output_1["SampleReceivedDate_Time"] = df_output_1.apply(lambda row: get_datetime(
    row["Received Date\n(YYYY-MM-DD)"],
    row["Received Time\n(24-hour format)"],
), axis = 1)

# Add "AccessionOrderID" column, rename some columns, and select the desired columns for output.
df_output_1["AccessionOrderID"] = ""
df_output_1 = df_output_1.rename(columns={"Accession No. or\nClient Sample ID": "AccessionNumber"})
df_output_1 = df_output_1[[
    "AccessionOrderID",
    "SampleReceivedDate_Time",
    "AccessionNumber",
]]

# Write df_output_1 to "SampleReceiptFile.csv".
df_output_1.to_csv(r"SampleReceiptFile.csv")
print("Successfully written df_output_1 to 'SampleReceiptFile.csv'.")



# Creating df that will be the second output file.
df_output_2 = df_vector

# Add "AccessionOrderID" column, rename some columns, and select the desired columns for output.
df_output_2["AccessionOrderID"] = ""
df_output_2 = df_output_2.rename(columns={
    "Received Date\n(YYYY-MM-DD)": "SampleReceiptDate",
    "Reported Date\n(YYYY-MM-DD)": "ResultCompleteDate",
    "Result\n(Detected / Undetected)": "Result"
})
df_output_2 = df_output_2[[
    "AccessionOrderID",
    "Result",
    "SampleReceiptDate",
    "ResultCompleteDate",
]]

# Write df_output_2 to "SampleResultResponse.csv".
df_output_2.to_csv(r"SampleResultResponse.csv")
print("Successfully written df_output_1 to 'SampleResultResponse.csv'.")



# Establish SFTP connection to destination remote server.
ssh.connect(hostname=env("DESTINATION_HOSTNAME"), username=env("DESTINATION_USERNAME"), password=env("DESTINATION_PASSWORD"), port=22)
print("SSH connection to destination complete.")
sftp_client = ssh.open_sftp()
print("SFTP connection to destination complete.")

# Upload "SampleReceiptFile.csv" and "SampleResultResponse.csv" to destination remote server.
sftp_client.put("SampleReceiptFile.csv", env("DESTINATION_FILE_PATH_1"))
sftp_client.put("SampleResultResponse.csv", env("DESTINATION_FILE_PATH_2"))
print("Successfully uploaded 'SampleReceiptFile.csv' and 'SampleResultResponse.csv' to destination remote server.")

# Close SFTP connection to destination remote server.
sftp_client.close()
print("SFTP connection to destination terminated.")
ssh.close()
print("SSH connection to destination terminated.")

print('\nOperation complete.')