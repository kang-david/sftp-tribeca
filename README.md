A script that

    1. downloads csv/xlsx files from Tribeca and Vector Health's SFTP servers using Paramiko,
    2. modifies the contents with Pandas, and
    3. uploads the output csv files to a destination remote server.

Once you have installed the dependencies and set .env variables, simply run "run.py" in the console.